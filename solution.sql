-- a. Find all artists that has the letter D in it's name.

SELECT name FROM artists WHERE name LIKE '%D' AND  name LIKE 'D%' AND name LIKE '%d%';

-- b. Find all songs that has a length of less than 230

SELECT song_name FROM tbl_songs WHERE length < 230;

-- c. Joins the 'albums' and 'tbl_songs' table.(only show the album name, song.name and song length)

SELECT albums.album_title, tbl_songs.song_name, tbl_songs.length FROM albums JOIN tbl_songs ON albums.id = tbl_songs.album_id;

-- d. Join the artists and albums tables. (find all albums that has letter a in its name).

SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE name LIKE '%a%';

-- e. Sort the albums in Z-A order. Show only the first 4 records.

SELECT album_title FROM albums ORDER BY album_title DESC LIMIT 4;

-- f. join the albums and songs tables.(sort albums from Z-A and sort songs from A-Z).

SELECT albums.album_title, tbl_songs.song_name FROM albums JOIN tbl_songs ORDER BY DESC;
SELECT albums.album_title, tbl_songs.song_name FROM tbl_songs JOIN albums ORDER BY ASC;