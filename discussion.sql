-- add new records

-- add 5 artists, 2 albums each, 2 songs per album.

INSERT INTO artists(name) VALUES ("Taylor Swift");
INSERT INTO artists(name) VALUES ("Lady Gaga");
INSERT INTO artists(name) VALUES ("Justin Bieber");
INSERT INTO artists(name) VALUES ("Ariana Grande");
INSERT INTO artists(name) VALUES ("Bruno Mars");


-- Taylor Swift
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Fearless", "2008", 3);

INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("Fearless", 246, "Pop",3);

INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("Love Story", 213, "Country-Pop",3);

INSERT INTO albums(album_title,date_released,artist_id) VALUES ("Red", "2012-03-20" ,3);

INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("State of Grace", 273, "Rock, Alternative Rock, Arena Rock",4);

INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("Red", 204, "Country",4);

-- Lady Gaga 
INSERT INTO albums(album_title,date_released,artist_id) VALUES ("A star is Born", "2018-06-24", 4);

INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("Black eyes", 221, "Rock and Roll", 5);
INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 5);

INSERT INTO albums(album_title,date_released,artist_id) VALUES ("Born this Way", "2011-03-18", 4);

INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("Born This Way", 252, "Electropop", 6);
INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("Hair", 320, "Electropop, rock", 6);

UPDATE tbl_songs SET length = 313 WHERE song_name = "State of Grace";

-- Justin Bieber

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Purpose", "2015-04-10", 5);

INSERT INTO tbl_songs(song_name, length, genre, album_id) VALUES ("Sorry", 232, "Dancehall-poptropical", 7);

INSERT INTO albums(album_title, date_released, artist_id) VALUES("Believe", "2012-12-20", 5);

INSERT INTO tbl_songs(song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 8);

-- Ariana Grande

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-08-14", 6);

INSERT INTO tbl_songs(song_name, length, genre, album_id) VALUES ("Into you", 242, "EDM House", 9);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-11-20", 6);

INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("Thank U, Next", 236, "Pop, R&B", 10);

-- Bruno Mars

INSERT INTO albums(album_title, date_released, artist_id) VALUES("24K Magic", "2016-04-24", 7);

INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("24K Magic", 207, "Funk, Disco, R&B", 11);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-10-17", 7);

INSERT INTO tbl_songs(song_name,length,genre,album_id) VALUES ("Lost", 232, "Pop", 12);

-- Exclude records

SELECT * FROM tbl_songs WHERE id != 11;

-- Greater or less than

SELECT * FROM tbl_songs WHERE id < 11;

SELECT * FROM tbl_songs WHERE id >= 11;

-- get specific IDs (OR)

SELECT * FROM tbl_songs WHERE id = 1 OR id = 3 OR id = 5;

-- get specific IDs (IN)

SELECT * FROM tbl_songs WHERE id IN (1, 3, 5);

-- return records in songs with genre of pop and kpop

SELECT * FROM tbl_songs WHERE genre IN("Pop", "Kpop");


-- combining conditions

SELECT * FROM tbl_songs WHERE album_id = 4 AND id < 8;

-- find partial matches

SELECT * FROM tbl_songs WHERE song_name LIKE '%a'; -- selects entry with letter at the end of %.


SELECT * FROM tbl_songs WHERE song_name LIKE 'a%'; -- selects entry with letter at the beginning of %.

SELECT * FROM tbl_songs WHERE song_name LIKE '%a%'; -- selects entry with letter where letter is in between.

SELECT * FROM albums WHERE date_released LIKE "%201%";

-- sorting records
SELECT * FROM tbl_songs ORDER BY song_name ASC;
SELECT * FROM tbl_songs ORDER BY song_name DESC;

-- getting distinct records
SELECT DISTINCT genre FROM tbl_songs;

-- Combine artists and albums tables
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- combine more than two TABLES
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

-- select columns to be included per tables

SELECT artists.name, albums.album_title FROM artists JOIN albums ON artists.id = albums.artist_id;